import { ADD_TO_CART,
	INCREASE_QUANTITY,
	DECREASE_QUANTITY,
	REMOVE_FROM_CART
	} from "./../store/actions"

export const initialState = {
	cart : {

	}
};

export const actionHandler = {
	ADD_TO_CART : (state, data) => {
		let { itemId } = data;
		let newVal = 1;
		if (state[itemId] !== undefined) {
			newVal = state[itemId].quantity + 1;
		}
		return {
			...state,
			[itemId] : {
				...state[itemId],
				quantity: newVal
			}
			};
	},
	INCREASE_QUANTITY: (state, data) => {
		let { itemId } = data;
		let	newVal = state[itemId].quantity + 1;
		return {
			...state,
			[itemId] : {
				...state[itemId],
				quantity: newVal
			}
			};

	},
	DECREASE_QUANTITY: (state, data) => {
		let { itemId } = data;
		let	newVal = state[itemId].quantity - 1;
		if (newVal < 0) { return { ...state }; }
		return {
			...state,
			[itemId] : {
				...state[itemId],
				quantity: newVal
			}
			};

	},
	REMOVE_FROM_CART: (state, data) => {
		let { itemId } = data;
		let newState = { ...state };
		delete newState[itemId];
		return newState;
	},

}

export default function cartReducer (state = initialState, action) {
	return action.type in actionHandler ? actionHandler[action.type](state, action.data) : state;
}