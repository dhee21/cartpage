import { ADD_TO_CART } from "./../store/actions"

export const initialState = {
	items: [],
	itemsById: {}
};

export const actionHandler = {
	
}

export default function productReducer (state = initialState, action) {
	return action.type in actionHandler ? actionHandler[action.type](state, action.data) : state;
}